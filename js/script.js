// 04052018 - fix tag issue
function generateIframe(id){
	var iframe = document.createElement('iframe');
		iframe.src = 'https://mmapi.ikea.com/embed/?item_number='+ id +'&amp;jsapi=1';
		iframe.frameborder = "0";
		iframe.width = "560px";
		iframe.height = "315px";
		iframe.allowfullscreen = "";
		iframe.scrolling = "no";
		document.getElementById("video-responsive").appendChild(iframe);
}
function getQuery(str){
	var path = location.search.substring(1),
		query = path.split('&'),
		getq,
		queries = {
			path: path
		};
	for (var i = query.length - 1; i >= 0; i--) {
		getq = extract(query[i]);
		queries[getq.key] = getq.val;
	}
	if(!str && path === '')
		return 'q=emptyuri';
	return (str)? path: queries;
}
function extract(a){
	var b = a.split('=');
	return {
		key: b[0],
		val: b[1]
	}
}
function redirect(q, test){
	var is_mobile = isMobile(),
		redirect, cid = {},
		mobile = 'https://m.ikea.com/'+ q.cc +'/en/catalog',
		desktop = 'https://www.ikea.com/'+ q.cc +'/en/catalog',
		v = parseInt(q.v);
	switch(v){
		case 1: // pip
			redirect = (is_mobile)? mobile +'/products/'+ q.itype +'/'+ q.id +'/': desktop +'/products/'+ idSPR(q);
		break;
		case 2: // family
			if(parseInt(q.id) === 11703)
				redirect = (is_mobile)? mobile +'/systems/10364/10397/11703/': desktop +'/categories/departments/'+ q.room +'/'+ q.id +'/';
			else if(parseInt(q.id) === 11794)
				redirect = (is_mobile)? mobile +'/systems/10365/10475/11794/?chapterId=14885': desktop +'/categories/departments/'+ q.room +'/'+ q.id +'/';
			else
				redirect = (is_mobile)? mobile +'/'+ q.family +'/'+ q.id +'/': desktop +'/categories/departments/'+ q.room +'/'+ q.family +'/'+ q.id;
		break;
		case 3: // video
			redirect = 'video.html?id='+ q.id +'&campaign='+ q.c;
		break;
		case 4: // shopping list
			// redirect = 'video.html?id='+ q.id +'&campaign='+ q.c;
		break;
		case 5: // store app
			var appstore = [
				'https://play.google.com/store/apps/details?id=com.ikea.kompis&insrc=storeappqr',
				'https://itunes.apple.com/'+ q.cc +'/app/ikea-store/id976577934?insrc=storeappqr'
			];
			redirect = appstore[parseInt(q.id) - 1];
		break;
		default: // 404
			redirect = '404.html?'+ queries;
		break;
	}
	if(test)
		return redirect + getCID(q.cc, v);
	window.location.href = redirect + getCID(q.cc, v);
}
function getCID(country, type){
	/*
		refer q.v
		CID generated from http://saint.countquestservices.se/
	*/
	var cid = {};
	switch(type){
		case 1: // pip
			cid = {
				sg: '?cid=ot|sg|qrcode|201804250547060921_5',
				my: '?cid=ot|my|qrcode|201804250547060921_6',
				th: '?cid=ot|th|qrcode|201804250547060921_7'
			}
		break;
		case 2: // family
			cid = {
				sg: '?cid=ot|sg|qrcode|201804250547060921_2',
				my: '?cid=ot|my|qrcode|201804250547060921_3',
				th: '?cid=ot|th|qrcode|201804250547060921_4'
			}
		break;
		case 3: // video
			cid = {
				sg: '&cid=ot|sg|qrcode|201804250547060921_11',
				my: '&cid=ot|my|qrcode|201804250547060921_12',
				th: '&cid=ot|th|qrcode|201804250547060921_13'
			}
		break;
		case 4: // shopping list
			cid = {
				sg: '&cid=ot|sg|qrcode|201804250547060921_17',
				my: '&cid=ot|my|qrcode|201804250547060921_18',
				th: '&cid=ot|th|qrcode|201804250547060921_19'
			}
		break;
		case 5: // store app
			cid = {
				sg: '&cid=ot|sg|qrcode|201804250547060921_8',
				my: '&cid=ot|my|qrcode|201804250547060921_9',
				th: '&cid=ot|th|qrcode|201804250547060921_10'
			}
		break;
		default: // 404
			cid = {
				sg: '&cid=ot|sg|qrcode|201804250547060921_14',
				my: '&cid=ot|my|qrcode|201804250547060921_15',
				th: '&cid=ot|th|qrcode|201804250547060921_16'
			}
		break;
	}
	return cid[country];
}
function idSPR(item){
	var s = "00000000" + item.id;
		s = s.substr(s.length - 8);
	return (item.itype == 'SPR')? 'S'+ s: s;
}
function isMobile(){
	return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))? true: false;
}
function getWebservice(url, callback, target, params){
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	xhr.onload = function (e) {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				callback(this.responseText, target, params);
			} else {
				console.error(xhr.statusText);
			}
		}
	};
	xhr.onerror = function (e) {
		console.error(xhr.statusText);
	};
	xhr.send(null);
}
function postWebservice(url, callback, target, params){
	var xhr = new XMLHttpRequest();
	xhr.onload = function (e) {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				callback(this.responseText, target, params);
			} else {
				console.error(xhr.statusText);
			}
		}
	};
	xhr.onerror = function (e) {
		console.error(xhr.statusText);
	};
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send(params);
}