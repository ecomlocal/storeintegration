# README #


### Tracking codes & stuff ###

### URI Codes ###
* c = Campaign
* v = View
* id = Product ID or Identifier
* cc = Country

### Campaign Codes (c) ###
* 01 = qrcodes
* 02 = shoppinglist
* 03 = storeapp

### View Codes (v) ###
* 01 = products
* 02 = family
* 03 = videos
* 04 = shoppinglist
* 05 = storeapp

### Country (cc) ###
* sg = Singapore
* my = Malaysia
* th = Thailand




### Video API ###
https://mmapi.ikea.com/retail/motionmedia/v1/item?status=active

### Video URL ###
https://mmapi.ikea.com/embed/?item_number='+ partnumber +'&amp;jsapi=1

### QR API ###
http(s)://api.qrserver.com/v1/create-qr-code/?data=[URL-encoded-text]&size=[pixels]x[pixels]
http://api.qrserver.com/v1/create-qr-code/?data=http%3A%2F%2Fwww.ikea.com%2Fsg%2Fen%2F&size=500x500


### heartbeat: ###
//www.ikea.com/sg/en/storeintegration/index.html?campaign=qrcode&country=sg&view=video&id=39217824
//www.ikea.com/sg/en/storeintegration/index.html?campaign=qrcode&country=sg&view=pip&id=39217824
//www.ikea.com/sg/en/storeintegration/index.html?campaign=qrcode&country=sg&view=family&id=37119

### video page: ###
//www.ikea.com/sg/en/storeintegration/video.html?id=39217824

### PIP page: ###
is_mobile:
//m.ikea.com/sg/en/catalog/products/spr/39217824/
else
//www.ikea.com/sg/en/catalog/products/S39217824/


### Series page: ###
is_mobile:
//m.ikea.com/sg/en/catalog/collections/37119/
else
//www.ikea.com/sg/en/catalog/categories/departments/living_room/collections/37119/
except for system, generate manually:

IVAR - 11703 - https://m.ikea.com/sg/en/catalog/systems/10364/10397/11703/
Besta - 11794 - https://m.ikea.com/sg/en/catalog/systems/10365/10475/11794/?chapterId=14885


### IKEA Store App Redirect: ###
Singapore
	Android - https://play.google.com/store/apps/details?id=com.ikea.kompis 
	IOS - https://itunes.apple.com/sg/app/ikea-store/id976577934 
Malaysia
	Android - https://play.google.com/store/apps/details?id=com.ikea.kompis 
	IOS - https://itunes.apple.com/my/app/ikea-store/id976577934 
Thailand
	Android - https://play.google.com/store/apps/details?id=com.ikea.kompis
	IOS - https://itunes.apple.com/th/app/ikea-store/id976577934 